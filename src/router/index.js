import Vue from 'vue'
import Router from 'vue-router'
import homePage from '@/pages/homePage'
import referencePage from '@/pages/referencePage'
import aboutPage from '@/pages/aboutPage'

//people
import peopleGallery from '@/pages/peopleGallery.vue'

//events
import eventIntersection from '@/pages/events/intersection.vue'
import eventOne from '@/pages/events/eventOne.vue'
import eventTwo from '@/pages/events/eventTwo.vue'
import eventThree from '@/pages/events/eventThree.vue'
import eventFour from '@/pages/events/eventFour.vue'

//travel
import travelIntersection from '@/pages/travel/intersection.vue'
import travelOne from '@/pages/travel/travelOne.vue'


import contactPage from '@/pages/contactPage'
import lightRoomTest from '@/pages/lightRoomTest'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'homePage',
      component: homePage
    },
    {
      path: '/references',
      name: 'referencePage',
      component: referencePage
    },
    {
      path: '/about',
      name: 'aboutPage',
      component: aboutPage
    },
    {
      path: '/contact',
      name: 'contactPage',
      component: contactPage
    },

    //people
    {
      path: '/people-gallery',
      name: 'peopleGallery',
      component: peopleGallery
    },

    //events
    {
      path: '/akce',
      name: 'eventIntersection',
      component: eventIntersection
    },
    {
      path: '/letna-1',
      name: 'eventOne',
      component: eventOne
    },
    {
      path: '/letna-2',
      name: 'eventTwo',
      component: eventTwo
    },
    {
      path: '/promoce',
      name: 'eventThree',
      component: eventThree
    },
    {
      path: '/korzo',
      name: 'eventFour',
      component: eventFour
    },

    //travel
    {
      path: '/mista',
      name: 'travelIntersection',
      component: travelIntersection
    },
    {
      path: '/neapol-a-sorrento',
      name: 'travelOne',
      component: travelOne
    },
    {
      path: '/light-room',
      name: 'lightRoomTest',
      component: lightRoomTest
    },
  ],
  mode: 'history'
})
